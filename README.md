# openFactory Community
English | [简体中文](README_cn.md)

Welcome to openFactory Community.


## Introduction

The Community repository stores all information about the openFactory community, including community governance, community activities, communication guidelines, and more.

## What you can find here

1. PLC solution. The PLC warehouse provides different brands of PLC data and corresponding library files (including but not limited to SV positioning module, cylinder module), different brands of dmi touch screen data and templates, including but not limited to various brands of sv motors, dd motors, linear motors, sensors, etc.

2. Robot solutions. The Robot Warehouse mainly provides information and corresponding usage plans of various brands of industrial robots.

3. Machine-Vision solution. The Machine-Vision Warehouse mainly provides two solutions: openVision and openMoveVision, which are based on pyqt+opencv to create an industrial-grade inspection system, and based on halcon+motor+c# visual positioning system.

4. CIM system. It is mainly to create an equipment management platform eap-mgr, which is used to manage and maintain equipment, and realize the self-operation of equipment based on the platform; later, it will also do big data analysis and training based on platform data; usage scenarios include but are not limited to alarms for detecting equipment Perception and self-recovery, etc.

![1682656484338](docs/imge/1682656484338.png)



## Contact

Mail: tom_toworld@163.com


