# openFactory社区
[English](README.md) | 简体中文


欢迎来到openFactory社区。


## 介绍

Community仓库保存了关于openFactory社区的所有信息，包括社区治理、社区活动、沟通交流指南等内容。 


## 这里有什么？

openFactory社区致力于提出智慧工厂解决方案，你在这里可以找到合适的方案来提高你的工厂效率，我们主要从以下方面解决你的问题：

1.PLC解决方案。PLC仓库提供不同品牌的PLC资料和相应的库文件（包含但不限于SV定位模块、气缸模块），不同品牌的dmi触摸屏资料和模板，包含但不限于各种品牌的sv马达，dd马达，线性马达，传感器等。

2.Robot解决方案。Robot仓库主要提供各种品牌工业机器人的资料和相应的使用方案。

3.Machine-Vision解决方案。Machine-Vision仓库主要提供openVision和openMoveVision两种解决方案，分别是基于pyqt+opencv打造工业级检测系统，基于halcon+motor+c#的视觉定位系统。

4.CIM系统。主要是打造设备管理平台eap-mgr，用于管理和维护设备，实现基于平台的设备自运转；后期也会基于平台数据做大数据分析，训练；使用场景包含但不限于用于探测设备的报警感知和自恢复等。

![1682656484338](docs/imge/1682656484338.png)



## 联系方式

Mail: tom_toworld@163.com
